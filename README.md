# NAME

PDL::FuncND - N dimensional version of functions

# VERSION

version 0.14

# SYNOPSIS

    use PDL::FuncND;

# DESCRIPTION

This module provides multi-dimensional implementations of common
functions.

# INTERNALS

# FUNCTIONS

## cauchyND

Evaluate the multi-variate Cauchy function on an N-dimensional grid or
at a set of locations.

    $a = cauchyND( [OPTIONAL TYPE], $nx, $ny, ..., \%options );
    $b = cauchyND( $a, \%options );
    cauchyND( inplace $a, \%options );
    $a->inplace->cauchyND( \%options );

**cauchyND** can evaluate the function either on a grid or at discrete
locations:

- evaluation on a grid

    Either specify the output piddle dimensions explicitly,

        $f = cauchyND( [ OPTIONAL TYPE], $nx, $ny, ..., \%options );

    or specify a template piddle _without_ specifying the `vectors`
    option:

        $f = cauchyND( $piddle, \%options );

    By default **cauchyND** will evaluate the function at the _indices_ of
    the points in the input piddle.  These may be mapped to other values
    by specifying a transform with the `transform` option.  **cauchyND**
    is inplace aware, and will use **$piddle** as the output piddle if its
    inplace flag is set.

        cauchyND( inplace $f, \%options );
        $f->inplace->cauchyND( \%options );

- evaluation at a set of locations

    The input piddle should represent a set of vectors and should have a
    shape of (N,m), where `m` is the number of vectors in the set. The
    `vectors` option must also be set:

        $piddle = pdl( [2,1], [3,1], [4,2]  );
        $f = cauchyND( $piddle, { vectors => 1 } );

    The vectors may be transformed before use via the `transform` option.

The following options are available:

- `center` | `centre`

    The center of the distribution.  If not specified it defaults to the
    origin.

    This may take one of the following forms

    - A vector of shape (N).

        The location of the center. This may be either a Perl arrayref or a
        one dimensional piddle.  If the input coordinates are transformed,
        this is in the _transformed_ space.

    - the string `auto`

        If the PDF is calculated on a grid, this will center the distribution on
        the grid. It is an error to use this for explicit locations.

    - An arrayref

        The first element of the array is a string indicating the meaning of
        the rest of the array.  The following are supported:

        - `offset`

            The second element of the array is a piddle indicating an offset from
            an automatically generated center.  This allows easily accumulating
            multiple offset offsets.  For example:

                $img  = cauchyND( double, 32, 32, { %attr, center => 'auto' } );
                $img += moffatND( $img, { %moffat_attr,
                                          center => [ offset => [ 5.24, 0.3 ] ] } );

- `transform`

    A PDL::Transform object to be applied to the input coordinates.

- `scale`

    The scale. If the input coordinates are transformed
    via the `transform` option, the units of scale are those in the
    _transformed_ space.  This may be specified as:

    - a scalar (Perl or piddle)

        This results in a symmetric distribution with the given scale along each
        coordinate.

    - a vector of shape (N) (piddle or Perl arrayref)

        This results in a distribution with the specified scales for each
        coordinate.

    - a matrix (piddle of shape (N,N))

        This should be a positive-definite matrix containing squared
        scales.

- `theta` (Perl scalar)

    **Only for 2D!** Applies a rotation (clockwise, e.g. +X
    rotates towards -Y) by the specified angle (specified in radians).

- `log` (Boolean)

    If true, return the logarithm of the function. Defaults to false.

## gaussND

Evaluate the sampled multi-dimensional Gaussian PDF on an
N-dimensional grid or at a set of locations.

    $f = gaussND( [OPTIONAL TYPE], $nx, $ny, ..., \%options );
    $f = gaussND( $piddle, \%options );
    gaussND( inplace $piddle, \%options );
    $a->inplace->gaussND( \%options );

**gaussND** can evaluate the function either on a grid or at discrete
locations:

- evaluation on a grid

    Either specify the output piddle dimensions explicitly,

        $f = gaussND( [ OPTIONAL TYPE], $nx, $ny, ..., \%options );

    or specify a template piddle _without_ specifying the `vectors`
    option:

        $f = gaussND( $piddle, \%options );

    By default **gaussND** will evaluate the function at the _indices_ of
    the points in the input piddle.  These may be mapped to other values
    by specifying a transform with the `transform` option.  **gaussND** is
    inplace aware, and will use **$piddle** as the output piddle if its
    inplace flag is set.

        gaussND( inplace $f, \%options );
        $f->inplace->gaussND( \%options );

- evaluation at a set of locations

    The input piddle should represent a set of vectors and should have a
    shape of (N,m), where `m` is the number of vectors in the set. The
    `vectors` option must also be set:

        $piddle = pdl( [2,1], [3,1], [4,2]  );
        $f = gaussND( $piddle, { vectors => 1 } );

    The vectors may be transformed before use via the `transform` option.

The following options are available:

- `center` | `centre`

    The center of the distribution.  If not specified it defaults to the
    origin.

    This may take one of the following values:

    - A vector of shape (N).

        The location of the center. This may be either a Perl arrayref or a
        one dimensional piddle.  If the input coordinates are transformed,
        this is in the _transformed_ space.

    - the string `auto`

        If the PDF is calculated on a grid, this will center the distribution on
        the grid. It is an error to use this for explicit locations.

- `transform`

    A PDL::Transform object to be applied to the input coordinates.

- `scale`

    The scale. If the input coordinates are transformed
    via the `transform` option, the units of scale are those in the
    _transformed_ space.  This may be specified as:

    - a scalar (Perl or piddle)

        This results in a symmetric distribution with the given scale along each
        coordinate.

    - a vector of shape (N) (piddle or Perl arrayref)

        This results in a distribution with the specified scales for each
        coordinate.

    - the full covariance matrix (piddle of shape (N,N))

        This results in a distribution with correlated scales. At present this
        matrix is not verified to be a legitimate covariance matrix.

- `theta` (Perl scalar)

    **Only for 2D!** Applies a rotation (clockwise, e.g. +X
    rotates towards -Y) by the specified angle (specified in radians).

- `log` (Boolean)

    If true, return the logarithm of the function. Defaults to false.

## lorentzND

Evaluate the multi-dimensional Lorentz function on an
N-dimensional grid or at a set of locations.

    $f = lorentzND( [OPTIONAL TYPE], $nx, $ny, ..., \%options );
    $f = lorentzND( $piddle, \%options );
    lorentzND( inplace $piddle, \%options );
    $a->inplace->lorentzND( \%options );

The Lorentz function is usually defined in one dimension as.

                         2
                        g
    f(x; x0, g) = --------------
                         2    2
                  (x - x0)  + g

where _g_ is the half-width at half-max (HWHM).  The two dimensional
symmetric analogue (sometimes called the "radial Lorentz
function") is

                                      2
                                     g
    f(x, y; x0, y0, g) = --------------------------
                                 2           2    2
                         (x - x0)  + (y - y0)  + g

One can extend this to an asymmetric form by writing it as

                              1
    f(x; u, S) = ---------------------------
                        T    -1
                 (x - u)  . S  . (x - u) + 1

where _x_ is now a vector, _u_ is the expectation value of the
distribution, and _S_ is a matrix describing the N-dimensional scale
of the distribution akin to (but not the same as!) a covariance matrix.

For example, a symmetric 2D Lorentz with HWHM of _g_ has

         [  2     ]
         [ g   0  ]
    S =  [        ]
         [      2 ]
         [ 0   g  ]

while an elliptical distribution elongated twice as much along the
_X_ axis as the _Y_ axis would be:

         [     2      ]
         [ (2*g)   0  ]
    S =  [            ]
         [          2 ]
         [ 0       g  ]

**lorentzND** extends the Lorentz function to N dimensions by treating
_x_ and _u_ as vectors of length _N_, and _S_ as an _NxN_ matrix.

**Please note!** While the one dimensional Lorentz function is
equivalent to the one-dimensional Cauchy (aprt from, in this
formulation, the normalization constant), this formulation of the
multi-dimensional Lorentz function is **not** equivalent to the
multi-dimensional Cauchy!

It can evaluate the function either on a grid or at discrete
locations:

- evaluation on a grid

    Either specify the output piddle dimensions explicitly,

        $f = lorentzND( [ OPTIONAL TYPE], $nx, $ny, ..., \%options );

    or specify a template piddle _without_ specifying the `vectors`
    option:

        $f = lorentzND( $piddle, \%options );

    By default **lorentzND** will evaluate the function at the _indices_ of
    the points in the input piddle.  These may be mapped to other values
    by specifying a transform with the `transform` option.  **lorentzND** is
    inplace aware, and will use **$piddle** as the output piddle if its
    inplace flag is set.

        lorentzND( inplace $f, \%options );
        $f->inplace->lorentzND( \%options );

- evaluation at a set of locations

    The input piddle should represent a set of vectors and should have a
    shape of (N,m), where `m` is the number of vectors in the set. The
    `vectors` option must also be set:

        $piddle = pdl( [2,1], [3,1], [4,2]  );
        $f = lorentzND( $piddle, { vectors => 1 } );

    The vectors may be transformed before use via the `transform` option.

The following options are available:

- `center` | `centre`

    The center of the distribution.  If not specified it defaults to the
    origin.

    This may take one of the following values:

    - A vector of shape (N).

        The location of the center. This may be either a Perl arrayref or a
        one dimensional piddle.  If the input coordinates are transformed,
        this is in the _transformed_ space.

    - the string `auto`

        If the PDF is calculated on a grid, this will center the distribution on
        the grid. It is an error to use this for explicit locations.

- `transform`

    A PDL::Transform object to be applied to the input coordinates.

- `scale`

    The scale. If the input coordinates are transformed
    via the `transform` option, the units of scale are those in the
    _transformed_ space.  This may be specified as:

    - a scalar (Perl or piddle)

        This results in a symmetric distribution with the given scale along each
        coordinate.

    - a vector of shape (N) (piddle or Perl arrayref)

        This results in a distribution with the specified scales for each
        coordinate.

    - a matrix (piddle of shape (N,N))

        This should be a positive-definite matrix containing squared
        scales.

- `theta` (Perl scalar)

    **Only for 2D!** Applies a rotation (clockwise, e.g. +X
    rotates towards -Y) by the specified angle (specified in radians).

## moffatND

Evaluate the multi-dimensional Moffat distribution on an
N-dimensional grid or at a set of locations.

    $f = moffatND( [OPTIONAL TYPE], $nx, $ny, ..., \%options );
    $f = moffatND( $piddle, \%options );
    moffatND( inplace $piddle, \%options );
    $a->inplace->moffatND( \%options );

The Moffat distribution is usually defined in two dimensions as.

                                                             2    2
                                                2  -1       x  + y  -beta
    f(x, y, alpha, beta) := (beta - 1) (pi alpha  )    (a + -------)
                                                                  2
                                                             alpha

In astronomy this is also known (confusingly) as the beta function, and is
often expressed in radial form:

                                                 2
                          2 r (beta - 1)        r    -beta
    fr(r, alpha, beta) := -------------- (1 + ------)
                                   2               2
                              alpha           alpha

One can extend the Cartesion expression to an n-dimensional asymmetric
form by writing it as

    fn(x, u, S, alpha, beta) :=

         gamma(beta)        n/2    1/2  -1              T   -1         -beta
      ----------------- ( pi    |S|    )    (1 + (x - u) . S . (x - u))
            2 beta - n
      gamma(----------)
                2

where _n_ is the number of dimensions, _x_ is now a vector, _u_ is
the expectation value of the distribution, and _S_ is a matrix
describing the N-dimensional scale of the distribution akin to (but
not the same as!) a covariance matrix.

Note that the integral of the distribution diverges for `beta <= n/2`.

**moffatND** extends the Moffat function to N dimensions by treating
_x_ and _u_ as vectors of length _N_, and _S_ as an _NxN_ matrix.

It can evaluate the function either on a grid or at discrete
locations:

- evaluation on a grid

    Either specify the output piddle dimensions explicitly,

        $f = moffatND( [ OPTIONAL TYPE], $nx, $ny, ..., \%options );

    or specify a template piddle _without_ specifying the `vectors`
    option:

        $f = moffatND( $piddle, \%options );

    By default **moffatND** will evaluate the function at the _indices_ of
    the points in the input piddle.  These may be mapped to other values
    by specifying a transform with the `transform` option.  **moffatND** is
    inplace aware, and will use **$piddle** as the output piddle if its
    inplace flag is set.

        moffatND( inplace $f, \%options );
        $f->inplace->moffatND( \%options );

- evaluation at a set of locations

    The input piddle should represent a set of vectors and should have a
    shape of (N,m), where `m` is the number of vectors in the set. The
    `vectors` option must also be set:

        $piddle = pdl( [2,1], [3,1], [4,2]  );
        $f = moffatND( $piddle, { vectors => 1 } );

    The vectors may be transformed before use via the `transform` option.

The following options are available:

- `beta`

    The Moffat _beta_ parameter. Required.

- `center` | `centre`

    The center of the distribution.  If not specified it defaults to the
    origin.

    This may take one of the following values:

    - A vector of shape (N).

        The location of the center. This may be either a Perl arrayref or a
        one dimensional piddle.  If the input coordinates are transformed,
        this is in the _transformed_ space.

    - the string `auto`

        If the PDF is calculated on a grid, this will center the distribution on
        the grid. It is an error to use this for explicit locations.

- `transform`

    A PDL::Transform object to be applied to the input coordinates.

- `scale`

    The scale. If the input coordinates are transformed
    via the `transform` option, the units of scale are those in the
    _transformed_ space.  This may be specified as:

    - a scalar (Perl or piddle)

        This results in a symmetric distribution with the given scale along each
        coordinate.

    - a vector of shape (N) (piddle or Perl arrayref)

        This results in a distribution with the specified scales for each
        coordinate.

    - a matrix (piddle of shape (N,N))

        This should be a positive-definite matrix containing squared
        scales.

- `theta` (Perl scalar)

    **Only for 2D!** Applies a rotation (clockwise, e.g. +X
    rotates towards -Y) by the specified angle (specified in radians).

## mahalanobis

Calculate the Mahalanobis distance for one or more vectors

    Signature: ( x(n,m), s(n,n), [o]d(m), \%options )

    $d = mahalanobis( $v, $S, \%options );
    mahalanobis( $v, $S, $d, \%options );

The Mahalanobis distance of a multivariate vector (v) from a location
(u) with a covariance matrix (S) is defined as

    dm(x,u) = sqrt( (v-u)T S^-1 (v-u) )

The input piddle representing the vectors (`$v`) must have shape (N,m),
where `N` is the dimension of the vector space and `m` is the number
of vectors.

The input covariance matrix (`$S`) must have shape (N,N).  It is _not_
checked for validity.

The available options are:

- `center` | `centre`

    The vector from which the distance is to be calculated.  It must have shape (N).
    It defaults to the origin.

- `inverted`

    If true, the input matrix is the inverse of the covariance matrix.
    Defaults to false.

- `squared`

    if true, the returned values are the distances squared.

# SUPPORT

## Bugs

Please report any bugs or feature requests to bug-pdl-funcnd@rt.cpan.org  or through the web interface at: [https://rt.cpan.org/Public/Dist/Display.html?Name=PDL-FuncND](https://rt.cpan.org/Public/Dist/Display.html?Name=PDL-FuncND)

## Source

Source is available at

    https://gitlab.com/djerius/pdl-funcnd

and may be cloned from

    https://gitlab.com/djerius/pdl-funcnd.git

# AUTHOR

Diab Jerius <djerius@cpan.org>

# COPYRIGHT AND LICENSE

This software is Copyright (c) 2025 by Smithsonian Astrophysical Observatory.

This is free software, licensed under:

    The GNU General Public License, Version 3, June 2007
